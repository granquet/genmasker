// some comment
#ifndef foo
#define foo

// GENMASK(39, 21)
#define ref 0x000000ffffe00000
// BIT(1) | BIT(6)
#define bar				0x42
// GENMASK(19,12)
#define baz   0x00ff000
// BIT(9)
#define buz 0x200
// GENMASK(4,2)
#define boz		28
// (BIT(1) | BIT(6)
#define bor (0x42)
// GENMASK(19,16)
#define bur (0xf << 16)
#define text "some text"

#endif //foo
