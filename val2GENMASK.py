#!/usr/bin/env python3.10
import sys
from math import log2

def val2GENMASK(val):
	h = int(log2(val))
	l = (val & -val).bit_length() - 1
	return (h,l)

def val2BIT(val):
    bits = [i for i, e in enumerate(bin(val)[2:][::-1]) if e == '1']
    if len(bits) == 1:
        return "BIT("+str(bits[0])+")"
    bitmask = "BIT(" + ") | BIT(".join(map(str,bits)) + ")"
    return "("+bitmask+")"

def tryGENMASK(val):
    if(val == 0):
        return "0"

    mask = val2GENMASK(val)

    if val.bit_count() != 1+(mask[0] - mask[1]) or mask[0] == mask[1]:
        return val2BIT(val)

    else:
        return "GENMASK("+ str(mask[0]) + "," + str(mask[1]) +")"

try:
    with open(sys.argv[1]) as f:
        import re
        pattern = '^#define ([\w]*)(\s+)\(?((?:0x)?[\da-f]+(?:\s+<<\s+[\da-f]+)?)\)?$'
        for line in f:
            result = re.search(pattern, line)
            if result:
                converted = tryGENMASK(eval(result.group(3)))
                print('#define ' + result.group(1) + result.group(2) + converted)
            else:
                print(line, end='')

except:
    val = eval(sys.argv[1])
    converted = tryGENMASK(val)
    print(converted)
