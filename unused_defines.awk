#!/bin/awk -f
# usage: ./unused_defines.awk -v include_file=linux/drivers/phy/mediatek/phy-mtk-hdmi-mt8195.h linux/drivers/phy/mediatek/*.c | xargs printf -- "-e %s\n" | xargs grep -v linux/drivers/phy/mediatek/phy-mtk-hdmi-mt8195.h

BEGIN {
	count = 0;
	while ((getline record < include_file) > 0) {
		if(record ~ /#define/) {
			match(record, "[[:space:]]?#define[[:space:]]*([^[:space:]]*)", m);
			array[m[1]] = 0;
		}
	}
}
{
	content[++files] = $0
}
END {
	for (i in array) {
		for (n in content)
			if (match(content[n], i)) {array[i]++}
	}

	for (i in array) {
		if (!array[i])
			print i
	}
}
