# Usage:
# transform your pdf with pdttotext:
# $ pdftotext -layout Registers.pdf
# $ gawk -f gen_regs.awk -- Registers.txt > my_regs.h
#
# Expected text format for registers:
# 
#REG_NAME: (Address = 0x42 Reset = 32'h0)
#some_bit0     0           R/W
#some_bit1     1           R/W
#some_bit2     16          R/W
#some_bit3     17          R/W
#some_bitfield 31:18       R/W
#

BEGIN {
curr_reg = ""
}

match($0, /^(\w*): \(Address = 0x([[:xdigit:]]*)/, reg) {
	print "\n#define "reg[1]"_OFF 0x"reg[2]; curr_reg = reg[1]
}

match($0, /^(\w*)\s*([[:digit:]]*)(:)?([[:digit:]]*)?\s*(R|W|R\/W|INT|INT_EN)/, field) {
	printf "#define "curr_reg"_"toupper(field[1])
	if(field[3])
		print " GENMASK("field[2]","field[4]")"
	else
		print " BIT("field[2]")"
}
